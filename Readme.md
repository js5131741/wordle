### Wordle

1. Git clone the project

`git clone https://gitlab.com/js5131741/wordle.git`

2. Navigate to the project directory

`cd wordle`

3. Install necessary packages

`npm install`

4. Run the file to play the game.

`node wordle.js`

5. Alternatively, you can type the following command to play the game.

`npm run play`

6. To run the test cases using jest

`npx jest` or `npm test`
