/* eslint-disable import/extensions */
/* eslint-disable no-undef */
const wordcheck = require('../wordle.js');

const userInput = 'green';
const randomWord = 'water';

const expected = {
  0: '\x1B[31mg : Wrong letter\x1B[39m',
  1: '\x1B[33mr : Correct letter, wrong placement\x1B[39m',
  2: '\x1B[31me : Wrong letter\x1B[39m',
  3: '\x1B[32me : Correct\x1B[39m',
  4: '\x1B[31mn : Wrong letter\x1B[39m',
};

test('Correct', () => {
  expect(wordcheck(randomWord, userInput)).toEqual(expected);
});
