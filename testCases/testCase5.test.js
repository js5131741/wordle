/* eslint-disable import/extensions */
/* eslint-disable no-undef */
const wordcheck = require('../wordle.js');

const userInput = 'bunch';
const randomWord = 'punch';

const expected = {
  0: '\x1B[31mb : Wrong letter\x1B[39m',
  1: '\x1B[32mu : Correct\x1B[39m',
  2: '\x1B[32mn : Correct\x1B[39m',
  3: '\x1B[32mc : Correct\x1B[39m',
  4: '\x1B[32mh : Correct\x1B[39m',
};

test('Correct', () => {
  expect(wordcheck(randomWord, userInput)).toEqual(expected);
});
