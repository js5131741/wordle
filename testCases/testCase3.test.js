/* eslint-disable import/extensions */
/* eslint-disable no-undef */
const wordcheck = require('../wordle.js');

const userInput = 'kills';
const randomWord = 'yeast';

const expected = {
  0: '\x1B[31mk : Wrong letter\x1B[39m',
  1: '\x1B[31mi : Wrong letter\x1B[39m',
  2: '\x1B[31ml : Wrong letter\x1B[39m',
  3: '\x1B[31ml : Wrong letter\x1B[39m',
  4: '\x1B[33ms : Correct letter, wrong placement\x1B[39m',
};

test('Correct', () => {
  expect(wordcheck(randomWord, userInput)).toEqual(expected);
});
