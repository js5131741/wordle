/* eslint-disable no-inner-declarations */
/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
/* eslint-disable operator-linebreak */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-continue */
/* eslint-disable no-console */
/* eslint-disable no-plusplus */
/* eslint-disable quotes */
/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs').promises;
const prompt = require('prompt-sync')();
const colors = require('ansi-colors');

// function to read the text file
async function textintoarray(txtfile) {
  const data = await fs.readFile(txtfile, 'utf-8');
  return data.split('\n');
}

// function to extract a random word from a given array
function randomword(array) {
  const randomIndex = Math.floor(Math.random() * array.length);
  return array[randomIndex];
}

// function to get user word
function userInput() {
  const inputWord = prompt('Guess the word :');
  return inputWord;
}

// function to ask if the user wants to play again
function playAgain() {
  const playagain = prompt('Press Y/y to play again. Any other key to close.');
  return playagain;
}

function convertWordToObj(word) {
  // mapping letter to index
  const result = {};

  for (let index = 0; index < 5; index++) {
    result[index] = word[index];
  }

  return result;
}

function removeLetterFromObj(obj, letterToRemove) {
  // remove a letter from an object if it is found
  for (const key in obj) {
    if (obj[key] === letterToRemove) {
      delete obj[key];
      break;
    }
  }
}
// function to compare letters of the word vs userWord and give hints
function wordcheck(word, userWord) {
  const wordDict = convertWordToObj(word);
  const userWordDict = convertWordToObj(userWord);
  const result = {};
  for (let i = 0; i < 5; i++) {
    if (wordDict[i] === userWordDict[i]) {
      result[i] = colors.green(`${wordDict[i]} : Correct`);
      delete wordDict[i];
    }
  }
  for (let i = 0; i < 5; i++) {
    if (!Object.keys(result).includes(`${i}`)) {
      if (Object.values(wordDict).includes(userWordDict[i])) {
        result[i] = colors.yellow(
          `${userWordDict[i]} : Correct letter, wrong placement`,
        );
        removeLetterFromObj(wordDict, userWordDict[i]);
      } else {
        result[i] = colors.red(`${userWordDict[i]} : Wrong letter`);
      }
    }
  }
  return result;
}
// function for the logic
function game(array) {
  let newgame = true; // to start the first game
  while (newgame) {
    let i = 0;
    console.log('New Game!');
    const word = randomword(array); // get the random word for each game
    while (i < 6) {
      // 6 tries
      console.log(`\nTry number :${i + 1}`);
      const userWord = userInput(); // get user input
      if (!array.includes(userWord)) {
        // check whether its a valid word
        console.log('Please enter a valid 5 letter word. Try again!');
        continue;
      }
      if (userWord === word) {
        // check if its the word
        console.log('Well Played! You win!');
        break;
      } else {
        // if not, give hints
        const result = wordcheck(word, userWord);
        Object.values(result).forEach((item) => console.log(`${item}\n`));
      }
      i++;
    }
    if (i > 5) {
      // if more than 6 tries, give the correct word
      console.log(`You lose. The word is ${word}.`);
    }
    const play = playAgain(); // ask for play again
    if (play !== 'y' && play !== 'Y') {
      newgame = false; // declare the game as false to not run the game again
    }
  }
}
// main function for the game.
if (require.main === module) {
  async function main() {
    const array = await textintoarray('sgb-words.txt');
    game(array);
  }
  main();
}

module.exports = wordcheck;
